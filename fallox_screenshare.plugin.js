//META{"name":"screenshare","description":"千ﾑﾚﾚᵒﾒ Discord | Enables ScreenSharing within a voice channel.","author":"alpнa","version":"0.0.1","website":"https://gitlab.com/Ni1kko","source":"https://gitlab.com/Ni1kko/fallox-discord-screensharing/blob/master/fallox_screenshare.plugin.js"}*//
 
var screenshare = function(falloxSS)
{
    return class _ 
    { 
        //Plugin Detials
        getName()          { return "千ﾑﾚﾚᵒﾒ Discord | ScreenSharing" }
        getDescription()   { return "Enables screensharing within a server." }
        getAuthor()        { return "alpнa" }
        getVersion()       { return "0.0.1" }
        getSettingsPanel() { return "<h3>No Options Yet :(</h3>" }
        
        
        //Plugin Data
        load()          { }
        unload()        { }
        start()         { falloxSS = BDV2.WebpackModules.findByUniqueProperties(["isDeveloper"]);Object.defineProperty(falloxSS,"isDeveloper",{get:_=>1,set:_=>_,configurable:true}); }
        stop()          { falloxSS && Object.defineProperty(falloxSS,"isDeveloper",{get:_=>0,set:_=>{throw new Error("Error Happened. This incident will be reported");},configurable: true}); }
        onMessage()     { }
        onSwitch()      { }
        observer(e)     { }
    };
}();
